package com.jspiders.spring_project.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;



import movie.java.Movie;

public class MovieRepository {
	
	public void saveMovieDetails( Movie movie )
	{
		try {
			Configuration cfg = new Configuration();
			cfg.configure();
			SessionFactory sessionFactory = cfg.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(movie);
			transaction.commit();
		}
		catch(HibernateException e) {
		}		
		}
	public Movie findMovieById(Long id)
	{
		Configuration cfg = new Configuration();
		cfg.configure();
		SessionFactory sessionFactory = cfg.buildSessionFactory();
		Session session = sessionFactory.openSession();
		return session.get(Movie.class ,id);
	}
	public void updateRatingById(Long id ,String rating)
	{
		Movie d = findMovieById(id);
		if(d!=null)
		{
			d.setRating(rating);
			Configuration cfg = new Configuration();
			cfg.configure();
			SessionFactory SessionFactory = cfg.buildSessionFactory();
			Session session = SessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.merge(d);
			transaction.commit();
			System.out.println("update successful");
		}
		else {
			System.out.println("update is unsuccessful");
		}
	}
	
	public void deleteMovieById(Long id)
	{
		Movie d1=findMovieById(id);
				if(d1 == null )System.out.println("Delete unsuccessfull");
				else {
				Configuration cfg = new Configuration();
				cfg.configure();
				SessionFactory SessionFactory = cfg.buildSessionFactory();
				Session session = SessionFactory.openSession();
				Transaction transaction = session.beginTransaction();
				 session.delete(d1);
				 transaction.commit();
				 System.out.println("Delete Succcessfull");
			}

		
	}
	
	}


