package com.jspiders.spring_project.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import movie.java.Payment;

public class PaymentRepository 
{
	
	public void paymentDetails( Payment payment )
	{
		try {
			Configuration cfg = new Configuration();
			cfg.configure();
			SessionFactory sessionFactory = cfg.buildSessionFactory();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			
			session.save(payment);
			transaction.commit();
		}
		catch(HibernateException e) {
		}		
		}
	

}
