package com.jspiders.spring_project;

import java.util.Date;

import com.jspiders.spring_project.repository.MovieRepository;
import com.jspiders.spring_project.repository.PaymentRepository;

import movie.java.Movie;
import movie.java.Payment;

public class App 
{
    public static void main( String[] args )
    {
       Movie movie = new Movie();
       movie.setId(135L);
       movie.setName("Naruto");
       movie.setBudget(550D);
       movie.setRating("10");
       movie.setReleaseDate(new Date());
       MovieRepository movieRepository = new MovieRepository();
     //  movieRepository.saveMovieDetails(movie);
     // System.out.println(movieRepository.findMovieById(137L));
      //movieRepository.updateRatingById(123L, "8");
     // movieRepository.deleteMovieById(139L);
       
      Payment payment = new Payment();
      payment.setMovieId(201L);
      payment.setNumberOfTickets(2L);
      payment.setPrice(200D);
      payment.setShowDate(new Date());
      payment.setShowTime("12.00am");
      payment.setPaymentType("debit-card");
     payment.setTotalPrice(2L, 200D);
     PaymentRepository paymentRepository = new PaymentRepository();
     paymentRepository.paymentDetails(payment);
      
    }
}
