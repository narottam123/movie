package movie.java;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jspiders.constants.AppConstants;
@Entity
@Table(name=AppConstants.PAYMENT_INFO)
public class Payment implements Serializable
{
	@Id
	@Column(name="movieId")
	private Long movieId;
	@Column(name=" numberOfTickets")
	private Long numberOfTickets;
	@Column(name="Price")
	private Double Price;
	@Column(name="showDate")
	private Date showDate;
	@Column(name="showTime")
	private String showTime;
	@Column(name="paymentType")
	private String paymentType;
	@Column(name="totalPrice")
	private Double totalPrice;
	public Payment()
	{
		
	}
	@Override
	public String toString() {
		return "Payment [movieId=" + movieId + ", numberOfTickets=" + numberOfTickets + ", Price=" + Price
				+ ", showDate=" + showDate + ", showTime=" + showTime + ", paymentType=" + paymentType + ", totalPrice="
				+ totalPrice + "]";
	}
	public Long getMovieId() {
		return movieId;
	}
	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}
	public Long getNumberOfTickets() {
		return numberOfTickets;
	}
	public void setNumberOfTickets(Long numberOfTickets) {
		this.numberOfTickets = numberOfTickets;
	}
	public Double getPrice() {
		return Price;
	}
	public void setPrice(Double price) {
		Price = price;
	}
	public Date getShowDate() {
		return showDate;
	}
	public void setShowDate(Date showDate) {
		this.showDate = showDate;
	}
	public String getShowTime() {
		return showTime;
	}
	public void setShowTime(String showTime) {
		this.showTime = showTime;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Long numberOfTickets,Double Price ) {
		this.totalPrice = totalPrice;
	}
}
